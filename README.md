=== WooCommerce fattura24  ===
Contributors: cristianozanca
Tags: fattura24, fatture, cloud, woocommerce, bill
Requires at least: 4.6
Tested up to: 4.9
Requires PHP: 5.6
Stable tag: 0.5.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html


WooCommerce Fattura24

== Description ==

The WooCommerce Fattura24 plugin allows you to transform the orders received in your online store made with WooCommerce in Invoices on fattura24.it

* The **Free Version** manages IVA at 0 and 22%
* The **Premium Version** manages IVA at 0, 4%, 10%, 22% and sends email with attached invoice

[More detailed info here](https://woofatture.com/documentazione-fattura24//)

Try Fattura24 for free at [this address](https://www.fattura24.it/)

How does it work? = Select the order number from the drop-down menu, check in the preview that it is the right one and then send it to fattura24.it

the WooCommerce plugin fattura24 requires the API KEY
that can be found at Configurazione > App e servizi esterni > WooCommerce

== Changelog ==

= 0.5.0 =
* Initial release

== Upgrade Notice ==
* Initial release

== Installation ==

This section describes how to install the plugin and get it working.

1. Unzip `woo-fattura24.zip`
2. Upload the `woo-fattura24` directory (not its contents, the whole directory) to `/wp-content/plugins/`
3. Activate the plugin through the `Plugins` menu in WordPress

== Frequently Asked Questions ==

= I prezzi si devono mettere iva inclusa? =

Si è possibile impostare solo l'opzione "Sì, voglio inserire i prezzi comprensivi di imposte"

== Screenshots ==

1. Invio Riuscito!
2. Quando l'invio non riesce appare la motivazione
3. Ecco le fatture inviate

== Credits ==
* Huge thanks to LoicTheAztec, Pascal Knecht, Carlo Camusso
