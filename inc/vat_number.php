<?php


/*
 *
 * valori nei campi del checkout
 *
*/

function billing_fields_woofattura24( $fields_fattura24 ) {


    $fields_fattura24['billing_cod_fisc'] = array(
        'label'       => __('Codice Fiscale','woocommerce'),
        'placeholder' => __('Scrivere il Codice Fiscale','woocommerce'),
        'required'    => true,
        'class'       => array('piva-number-class form-row-wide' ),
        'clear'		  => true

    );

    $fields_fattura24['billing_partita_iva'] = array(
        'label'       => __('Partita Iva','woocommerce'),
        'placeholder' => __('Scrivere il numero di Partita Iva','woocommerce'),
        'required'    => false,
        'clear'       => true,
        'class'       => array( 'form-row' ),

    );

    return $fields_fattura24;
}

/*
 *
 * valori modificabili dell'ordine
 *
 * */



function admin_billing_field_woofattura24( $fields_fattura24 ) {

    $fields_fattura24['cod_fisc'] = array(
        'label' => __('Codice Fiscale','woocommerce'),
        'wrapper_class' => 'form-field-wide',
        'show' => true,

    );
    $fields_fattura24['partita_iva'] = array(
        'label' => __('Partita Iva','woocommerce'),
        'wrapper_class' => 'form-field-wide',
        'show' => true,

    );

    return $fields_fattura24;
}

