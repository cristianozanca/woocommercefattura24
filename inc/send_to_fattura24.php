<?php

// Don't access this directly, please
if (!defined('ABSPATH')) exit;




$api_uid = get_option('api_uid_fattura24');
$api_key = get_option('api_key_fattura24');


//$lista_articoli = array();


//$aggiungi_shipping = array();

//$spedizione_netta = $order_data['shipping_total'] - ($order_data['shipping_total'] - round(($order_data['shipping_total'] / 122) * 100, 2));
$spedizione_lorda = $order_data['shipping_total'] + $order_data['shipping_tax'] ;
$spedizione_netta = $spedizione_lorda - $order_data['shipping_tax'];

$shipping_rate = $item_data['taxes'];

global $codice_iva;

$codice_iva = '';


foreach ($order->get_items() as $item_key => $item_values):

    $item_data = $item_values->get_data();


    $line_total = $item_data['total'];


    $product_id = $item_values->get_product_id(); // the Product id
    $wc_product = $item_values->get_product(); // the WC_Product object
    ## Access Order Items data properties (in an array of values) ##
    $item_data = $item_values->get_data();
    $_product = wc_get_product($product_id);

    //$tax_rates = WC_Tax::get_rates($_product->get_tax_class());
	$tax_rates = WC_Tax::get_base_tax_rates( $_product->get_tax_class( true ) );



	//$tax_rate = reset($tax_rates);




    if (!empty($tax_rates)) {
		$tax_rate = reset($tax_rates);

    if ($tax_rate['rate'] == 22) {

        $codice_iva = 22;


    } elseif ($tax_rate['rate'] == 0) {

        $codice_iva = 0;

    } elseif ($tax_rate['rate'] == 4) {

      ?>

            <div id="message" class="notice notice-error is-dismissible">

                <p><?php echo __("4% Iva attiva esclusivamente con la ","woo-fattura24")?> <a href="https://woofatture.com/shop/woo-fattura24-premium-1-year-updates">
					    <b><?php echo __("Versione Premium","woo-fattura24")?> </a>  <?php #print $description; ?></b></p>
            </div>

            <script>
                jQuery("div#message").appendTo("div#top_fattura24");
            </script>

		    <?php return;

	    }


     elseif ($tax_rate['rate'] == 10) {

	    ?>

        <div id="message" class="notice notice-error is-dismissible">

            <p><?php echo __("10% Iva attiva esclusivamente con la ","woo-fattura24")?> <a href="https://woofatture.com/shop/woo-fattura24-premium-1-year-updates">
                    <b><?php echo __("Versione Premium","woo-fattura24")?> </a>  <?php #print $description; ?></b></p>
        </div>

        <script>
            jQuery("div#message").appendTo("div#top_fattura24");
        </script>

	    <?php return;

    }




	}


 //           $prezzo_singolo_prodotto = ((round($item_data['total'], 2)+$item_data['total_tax']) / $item_data['quantity']);
	        $ivatosiono = true;


	$lista_articoli [] = array (

		'Row' => array (
	    'Code' => 0000,
	    'Description' => $item_data['name'] .' Quantità '.$item_data['quantity'],
	    'Qty' => $item_data['quantity'],
	    'Um' => '',
	    'Price' => $item_data['subtotal']/$item_data['quantity'],
	    'Discounts' => '',
	    'VatCode' => $codice_iva,
	    /*'VatCode' => $item_data['subtotal_tax'],*/
	    'VatDescription' => 'IVA '.$codice_iva.'%'

		)
    );

//	        $lista_articoli [] = $articolo_ordine_f24;

 //  var_dump($lista_articoli);





if ($order_data['shipping_total'] > 0) {

	$costi_spedizione_f24 = array (

		'Row' => array (

	        'Code' => 0000,
            "Description" => "Spese di Spedizione",
            "Qty" => 1,
	        'Um' => '',
            'Price' => $spedizione_netta,
	        'Discounts' => '',

	        'VatCode' => 22 ,
	        'VatDescription' => 'IVA 22%'
			)

        );

	$lista_articoli ['Rows'] =  $costi_spedizione_f24 ;

}




endforeach;

// print_r($lista_articoli);



if ( 'fattura' == get_option('fattura24_send_choice') ) {

	$fattura_o_ricevuta =  "I-force";

	}

elseif ( 'ricevuta' == get_option('fattura24_send_choice') ) {

	$fattura_o_ricevuta =  "R";

}

elseif ( 'auto' == get_option('fattura24_send_choice') ){

	$fattura_o_ricevuta =  "I";

}

if ( 1 == get_option('fattura24_sendemail_choice') ) {

	$sendornot_email = 'true';

}

elseif ( 0 == get_option('fattura24_sendemail_choice') ) {

	$sendornot_email = 'false';
}

$order_array = array (
	'Document' => array (
		'DocumentType' => $fattura_o_ricevuta,
		'CustomerName' => $order_billing_first_name . " " . $order_billing_last_name ,
		'CustomerAddress' => $order_billing_address_1,
		'CustomerPostcode' => $order_billing_postcode,
		'CustomerCity' => $order_billing_city,
		'CustomerProvince' => $order_billing_state,
		'CustomerCountry' => $order_billing_country,
		'CustomerFiscalCode' => $order_billing_codfis,
		'CustomerVatCode' => $order_billing_partiva,
		'CustomerCellPhone' => $order_billing_phone,
		'CustomerEmail' => $order_billing_email,
		'DeliveryName' => $order_shipping_first_name . " " . $order_shipping_last_name,
		'DeliveryAddress' => $order_shipping_address_1,
		'DeliveryPostcode' => $order_shipping_postcode,
		'DeliveryCity' => $order_shipping_city,
		'DeliveryProvince' => $order_shipping_state,
		'DeliveryCountry' => $order_shipping_country,
		'Object' => "Ordine WooCommerce numero ".$id_ordine_scelto,
		'TotalWithoutTax' => $totale_esclusaiva,
		'PaymentMethodName' => $order_payment_method_title,
		'PaymentMethodDescription' => $order_payment_method,
		'VatAmount' => $order_total_tax,
		'Total' => $order_total,
		'FootNotes' => 'Vi ringraziamo per la preferenza accordataci',
		'SendEmail' => $sendornot_email,
		'UpdateStorage' => '1',
		'F24OrderId' => '12345',
		'IdTemplate' => '123',
		'CustomField1' => '',
		'CustomField2' => '',


		'Payments' => array (

			'Payment' => array (

				'Date' => $order_data['date_created']->date('Y-m-d'),
				'Amount' => $order_total,
				'Paid' => 'true',

			),
		),

		'Rows' => ($lista_articoli)

		),


);




require (plugin_dir_path( __FILE__ ) . '/create_doc_remote.php');



// echo "<pre>". htmlentities (arrayToXmlFattura24($order_array)) . "</pre>";


