<div id="woo_fattura24">
	<header></header>
</div>

<h2>Inserire l'API KEY di Fattura24.com che si trova in <i><span>CONFIGURAZIONE ->> App e Servizi Esterni ->> WooCommerce</span></i></h2>
	<hr>
	<?php

	/**
	 *
	 * Form for API Value API UID and API KEY
	 *
	 */
	?>

<table border="0" cellpadding="6">
    <tr>
        <td align="right">

	<form id="woo-fattura24-settings_key" method="POST">

		<?php wp_nonce_field(); ?>

		<label for="woo_fattura24_apikey">API KEY

            <?php

            require (plugin_dir_path( __FILE__ ) . '/api_test.php');

			?>

        </label>

		<input type="password" class="f24_pwd_textbox" name="api_key_fattura24" placeholder="api key"
		       value="<?php echo get_option('api_key_fattura24'); ?>">



        </td>
        <td>

            <input type="submit" value="Save" class="button button-primary button-large">
        </td>
	</form>

    </tr>


    <!-- #################### -->


    <tr>
        <td align="right">

            <form method="POST">
                <label for="fattura24_auto_save"><?php echo __( 'Abilita la creazione di', 'woo-fattura24' );?></label>

<br>

                <input type="radio" id="fattura" name="fattura24_send_choice" value="fattura"

					<?php

					if ( 'fattura' == get_option('fattura24_send_choice') )
					{ echo 'checked';}
					else { echo ''; }
					?> >
                <label for="contactChoice1"><?php echo __('FATTURA','woo-fattura24'); ?></label>

                <input type="radio" id="ricevuta" name="fattura24_send_choice" value="ricevuta"

					<?php

					if ( 'ricevuta' == get_option('fattura24_send_choice') )
					{ echo 'checked';}
					else { echo ''; }
					?>

                >
                <label for="contactChoice2"><?php echo __('RICEVUTA','woo-fattura24'); ?></label>

                <input type="radio" id="auto" name="fattura24_send_choice" value="auto"

		            <?php

		            if ( 'auto' == get_option('fattura24_send_choice') )
		            { echo 'checked';}
		            else { echo ''; }
		            ?>

                >
                <label for="contactChoice2"><?php echo __('AUTO (se c\'è PI Fattura) ','woo-fattura24'); ?></label>




        </td>
        <td>
            <input type="submit" value="Save" class="button button-primary button-large">
            </form>
        </td>
    </tr>

    <!-- #################### -->

    <tr>
        <td align="right">



            <form method="POST">
                <label for="fattura24_auto_save"><?php echo __( 'Abilita la creazione in <b>Automatico</b> quando l\'ordine è <b>Completato</b> ', 'woo-fattura24' );?></label>

                <input type="hidden" name="fattura24_auto_save" value="0" />
                <input type="checkbox" name="fattura24_auto_save" id="fattura24_auto_save" value="1" <?php if ( 1 == get_option('fattura24_auto_save') ) {echo 'checked';}

		        else {echo '';}

		        ?>>
        </td>
        <td>
            <input type="submit" value="Save" class="button button-primary button-large">
            </form>





        </td>
    </tr>


    <!-- #################### -->







    <tr>
        <td align="right">

<form method="POST">
	<label for="fattura24_partiva_codfisc"><?php echo __( 'Attiva la voce Partita Iva e Codice Fiscale nel Checkout di WooFattura24', 'woo-fattura24' );?></label>

	<input type="hidden" name="fattura24_partiva_codfisc" value="0" />
	<input type="checkbox" name="fattura24_partiva_codfisc" id="fattura24_partiva_codfisc" value="1" <?php if ( 1 == get_option('fattura24_partiva_codfisc') ) {echo 'checked';}

	else {echo '';}

	?>>
        </td>
        <td>
	<input type="submit" value="Save" class="button button-primary button-large">
</form>

        </td>
    </tr>


    <tr>
        <td>
            <b>In alternativa</b> è possibile utilizzare il plugin <i>WooCommerce P.IVA e Codice Fiscale per Italia</i>
        </td>

    </tr>
    <!-- #################### -->


</table>

<p>Compra la <a href="https://woofatture.com/shop/woo-fattura24-premium-1-year-updates">Versione Premium!</a></p>
<div id="promo_premium"></div>